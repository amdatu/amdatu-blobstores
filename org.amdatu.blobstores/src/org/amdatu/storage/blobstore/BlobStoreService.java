/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.storage.blobstore;

import java.util.Properties;

import org.jclouds.blobstore.BlobStoreContext;

/**
 * 
 * The BlobStoreService has the sole purpose of exposing the JClouds BlobStoreContext API.
 * Using this service is more flexible than direct usage however because it can be configured using Configuration Admin.
 */
public interface BlobStoreService {
    /**
     * Provides convenience constants for various storage providers supported by JClouds. 
     */
    public static enum Providers {
        /** In-memory provider */
        TRANSIENT("transient"),
        /** File system backed provider */
        FILESYSTEM("filesystem"),
        /** Eucalyptus PartnerCloud provider */
        EUCALYPTUS_PARTNERCLOUD_S3("eucalyptus-partnercloud-s3"),
        /** Synaptic Storage provider */
        SYNAPTIC_STORAGE("synaptic-storage"),
        /** Microsoft Azure */
        AZUREBLOB("azureblob"),
        /** CloudOne storage provider */
        CLOUDONESTORAGE("cloudonestorage"),
        /** CloudFiles US provider */
        CLOUDFILES_US("cloudfiles-us"),
        /** CloudFiles UK provider */
        CLOUDFILES_UK("cloudfiles-uk"),
        /** NineFold storage provider */
        NINEFOLD_STORAGE("ninefold-storage"),
        /** Amazon S3 provider */
        AWS_S3("aws-s3"),
        /** Google Storage provider */
        GOOGLESTORAGE("googlestorage"),
        /** ScaleUp storage provider */
        SCALEUP_STORAGE("scaleup-storage"),
        /** Host Europe storage provider */
        HOSTEUROPE_STORAGE("hosteurope-storage"),
        /** Tiscali storage provider */
        TISCALI_STORAGE("tiscali-storage");

        private final String m_id;

        Providers(String id) {
            m_id = id;
        }

        public String getAsServiceFilter() {
            return new StringBuilder("(provider=").append(m_id).append(")").toString();
        }

        public Properties getAsServiceProperties() {
            Properties props = new Properties();
            props.put("provider", m_id);
            return props;
        }

        @Override
        public String toString() {
            return m_id;
        }
    }

    /**
     * Returns the JClouds {@link BlobStoreContext} API.
     * <p>
     * The caller is responsible for closing the context. The returned context
     * is thread-safe and may be reused multiple times.
     * </p>
     * @return a {@link BlobStoreContext} instance, never <code>null</code>.
     */
    BlobStoreContext getContext();
}
