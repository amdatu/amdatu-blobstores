/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.storage.blobstore.osgi;

import static org.osgi.framework.Constants.SERVICE_PID;

import java.util.Dictionary;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.amdatu.storage.blobstore.BlobStoreService;
import org.amdatu.storage.blobstore.impl.BlobStoreServiceImpl;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.jclouds.filesystem.reference.FilesystemConstants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Service factory that configures BlobStoreService instances. A new instance is created for each provider.
 */
public class BlobStoreServiceFactory implements ManagedServiceFactory {
    public final static String PID = "org.amdatu.storage.servicefactory";

    public static final String ID_KEY = "id";
    public static final String PROVIDER_KEY = "provider";
    public static final String IDENTITY_KEY = "identity";
    public static final String SECRET_KEY = "secret";
    public static final String BASEDIR_KEY = "basedir";
    // Injected by Felix DM...
    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_log;

    private final ConcurrentMap<String, Component> m_components = new ConcurrentHashMap<String, Component>();

    public void deleted(String pid) {
        Component comp = m_components.remove(pid);
        if (comp != null) {
            m_dependencyManager.remove(comp);

            info("Unregistered service with pid: '" + pid + "'");
        }
    }

    public String getName() {
        return "org.amdatu.storage.blobstorefactory";
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void updated(String pid, Dictionary properties) throws ConfigurationException {
        String id = getMandatoryString(properties, ID_KEY);
        String provider = getMandatoryString(properties, PROVIDER_KEY);
        String identity = getOptionalString(properties, IDENTITY_KEY);
        String secret = getOptionalString(properties, SECRET_KEY);

        Properties overriddenProperties = null;
        // For a FileSystem provider, we need additional properties...
        if (isFilesystemProvider(provider)) {
            String basedir = getMandatoryString(properties, BASEDIR_KEY);

            overriddenProperties = new Properties();
            overriddenProperties.setProperty(FilesystemConstants.PROPERTY_BASEDIR, basedir);
        }

        Properties serviceProperties = new Properties();
        serviceProperties.setProperty(SERVICE_PID, pid);
        serviceProperties.setProperty(PROVIDER_KEY, provider);
        serviceProperties.setProperty(ID_KEY, id);
        if (identity != null) {
            serviceProperties.setProperty(IDENTITY_KEY, identity);
        }

        Component component = m_components.get(pid);
        if (component == null) {
            // Create a new one...
            BlobStoreServiceImpl impl = new BlobStoreServiceImpl();

            component = m_dependencyManager
                .createComponent()
                .setInterface(BlobStoreService.class.getName(), serviceProperties)
                .setImplementation(impl);

            try {
                // Update it, will complain with an exception if invalid settings are given...
                impl.updated(provider, identity, secret, overriddenProperties);
            } catch (Exception exception) {
                // Log some additional information, as it might not be trivial to relate the exception to the supplied configuration...
                error("Failed to create initial configuration for provider '" + provider + "', with id " + id);
                throw exception;
            }

            if (m_components.putIfAbsent(pid, component) == null) {
                m_dependencyManager.add(component);

                info("Registered BlobStoreService for provider '" + provider + "' with id " + id);
            }
        } else {
            // Update existing one, will complain with an exception if invalid settings are given...
            BlobStoreServiceImpl impl = component.getInstance();

            try {
                // Update it, will complain with an exception if invalid settings are given...
                impl.updated(provider, identity, secret, overriddenProperties);
            } catch (Exception exception) {
                // Log some additional information, as it might not be trivial to relate the exception to the supplied configuration...
                error("Failed to update configuration for provider '" + provider + "', with id " + id);
                throw exception;
            }

            component.setServiceProperties(serviceProperties);

            info("Updated BlobStoreService for provider '" + provider + "', with id " + id);
        }
    }

    protected String getMandatoryString(Dictionary<?, ?> properties, String key) throws ConfigurationException {
        String result = getOptionalString(properties, key);
        if (isEmpty(result)) {
            error("Error while registering new BlobStoreService: " + key + " is missing or an invalid value!");
            throw new ConfigurationException(key, "missing or invalid value!");
        }
        return result;
    }

    protected String getOptionalString(Dictionary<?, ?> properties, String key) throws ConfigurationException {
        Object value = properties.get(key);
        if (value == null) {
            return null;
        }
        if (!(value instanceof String)) {
            error("Error while registering new BlobStoreService: " + key + " is an invalid value!");
            throw new ConfigurationException(key, "invalid value!");
        }
        return ((String) value).trim();
    }

    private void error(String msg) {
        if (m_log != null) {
            m_log.log(LogService.LOG_ERROR, msg);
        }
    }

    private void info(String msg) {
        if (m_log != null) {
            m_log.log(LogService.LOG_INFO, msg);
        }
    }

    private boolean isEmpty(String value) {
        return value == null || "".equals(value.trim());
    }

    private boolean isFilesystemProvider(String provider) {
        return BlobStoreService.Providers.FILESYSTEM.toString().equals(provider);
    }
}
