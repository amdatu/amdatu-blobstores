/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.storage.blobstore.impl;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

import org.amdatu.storage.blobstore.BlobStoreService;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStoreContext;
import org.osgi.service.cm.ConfigurationException;

/**
 * Creates a BlobStoreContext for the provider, identity and secret configured by Configuration Admin.
 * Multiple instance of this service can coexist for using multiple blob store providers.
 * Each service instance has a property "provider" that is one of {@link BlobStoreService#Providers}.
 */
public class BlobStoreServiceImpl implements BlobStoreService {
    private final AtomicReference<BlobStoreContext> m_contextRef;

    public BlobStoreServiceImpl() {
        m_contextRef = new AtomicReference<BlobStoreContext>();
    }

    public BlobStoreContext getContext() {
        return m_contextRef.get();
    }

    public void updated(String provider, String identity, String secret, Properties overridenProperties) throws ConfigurationException {
        setContext(createContext(provider, identity, secret, overridenProperties));
    }

    protected BlobStoreContext createContext(String provider, String identity, String secret, Properties overridenProperties) {
        ClassLoader oldCL = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            
            ContextBuilder builder;
            if(identity != null && secret != null) {
                builder = ContextBuilder.newBuilder(provider).credentials(identity, secret);
            } else {
                builder = ContextBuilder.newBuilder(provider);
            }
            if (overridenProperties != null) {
                builder.overrides(overridenProperties);
            }
            return builder.buildView(BlobStoreContext.class);
        } finally {
            Thread.currentThread().setContextClassLoader(oldCL);
        }
    }

    protected void setContext(BlobStoreContext context) {
        BlobStoreContext old;
        do {
            old = m_contextRef.get();
        } while (!m_contextRef.compareAndSet(old, context));
        
        if (old != null) {
            // Make sure to close the old context...
            old.close();
        }
    }
}
