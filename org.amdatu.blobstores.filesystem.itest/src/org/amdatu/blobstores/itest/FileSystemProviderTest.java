/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstores.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import java.io.ByteArrayInputStream;
import java.io.File;

import junit.framework.TestCase;

import org.amdatu.storage.blobstore.BlobStoreService;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;

/**
 * The JClouds file system provider requires an additional property basedir.
 */
public class FileSystemProviderTest extends TestCase {
    
    private volatile BlobStoreService m_blobStoreService;
    
    private final String containerName = "testdir" + System.currentTimeMillis();
    private final String blobName = "testfile";

    private final File containerDir = new File(System.getProperty("java.io.tmpdir"), containerName);
    private final File createdFile = new File(containerDir, blobName);

    @Override
    public void setUp() throws Exception {
        configure(this)
            .add(
                createFactoryConfiguration("org.amdatu.storage.servicefactory").set("id", "test")
                    .set("provider", BlobStoreService.Providers.FILESYSTEM.toString())
                    .set("basedir", System.getProperty("java.io.tmpdir")))
            .add(createServiceDependency().setService(BlobStoreService.class).setRequired(true)).apply();
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        if (createdFile.exists()) {
            createdFile.delete();
        }
        if (containerDir.exists()) {
            containerDir.delete();
        }
        cleanUp(this);
        super.tearDown();
    }

    public void testCanBeConfigured() {
        BlobStoreContext bsContext = m_blobStoreService.getContext();

        assertNotNull(bsContext);
        assertNotNull(bsContext.getBlobStore());
    }

    public void testWriteFileOk() {
        BlobStoreContext bsContext = m_blobStoreService.getContext();
        BlobStore blobStore = bsContext.getBlobStore();

        // We've not created the container yet, so it should not exist on our local FS...
        assertFalse("Directory " + containerDir + " should NOT exist?!", containerDir.exists());

        blobStore.createContainerInLocation(null, containerName);

        // We've created the container, so it should exist on our local FS, but it should be empty...
        assertTrue("Directory " + containerDir + " should exist?!", containerDir.exists());
        assertFalse("File " + createdFile + " should NOT exist?!", createdFile.exists());

        ByteArrayInputStream bais = new ByteArrayInputStream("somecontent".getBytes());

        blobStore.putBlob(containerName, blobStore.blobBuilder(blobName).payload(bais).build());

        // The blob should be stored on our local FS by now...
        assertTrue("File " + createdFile + " should exist?!", createdFile.exists());
    }
}
