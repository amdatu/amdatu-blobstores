/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.blobstores.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.amdatu.testing.configurator.TestUtils.deleteConfiguration;
import static org.amdatu.testing.configurator.TestUtils.getConfiguration;
import static org.amdatu.testing.configurator.TestUtils.getFactoryConfiguration;

import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.storage.blobstore.BlobStoreService;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.PageSet;
import org.jclouds.blobstore.domain.StorageMetadata;
import org.jclouds.domain.Location;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;

import junit.framework.TestCase;

public class BlobStoreServiceTest extends TestCase {
    private String m_servicePID;
    private volatile ServiceListener m_listener;
    private final BundleContext context = FrameworkUtil.getBundle(BlobStoreServiceTest.class).getBundleContext();

    private volatile BlobStoreService instance;
    private Configuration m_blobstoreServiceFactoryConfiguration;

    @Override
    public void setUp() throws Exception {
        final String factoryPID = "org.amdatu.storage.servicefactory";

        Dictionary<String, Object> props = new Hashtable<>();
        props.put("id", "test");
        props.put("provider", BlobStoreService.Providers.TRANSIENT.toString());
        props.put("identity", "myId");
        props.put("secret", "mySecret");

        m_blobstoreServiceFactoryConfiguration = getFactoryConfiguration(factoryPID);
        m_blobstoreServiceFactoryConfiguration.update(props);
        m_servicePID = m_blobstoreServiceFactoryConfiguration.getPid();

        configure(this).add(
            createServiceDependency().setService(BlobStoreService.class, "(service.pid=" + m_servicePID + ")")
                .setRequired(true)).apply();
        super.setUp();
    }

    /**
     * Tests that deleting the configuration for a blob store causes it to be
     * unregistered and no longer be available as service.
     */
    public void testDeleteBlobStoreServiceConfigurationOk() throws Exception {
        final String containerName = "container" + System.nanoTime();

        ServiceReference<?>[] serviceRefs;

        serviceRefs =
            context.getServiceReferences(BlobStoreService.class.getName(), "(service.pid=" + m_servicePID + ")");

        assertNotNull("No service references found for service?!", serviceRefs);
        assertEquals(1, serviceRefs.length);

        final CountDownLatch removedLatch = new CountDownLatch(1);

        m_listener = new ServiceListener() {
            @Override
            public void serviceChanged(ServiceEvent event) {
                if (event.getType() == ServiceEvent.UNREGISTERING) {
                    ServiceReference<?> serviceRef = event.getServiceReference();

                    assertEquals("test", serviceRef.getProperty("id"));

                    removedLatch.countDown();
                }
            }
        };
        // keep track when our BlobStoreService goes away...
        context.addServiceListener(m_listener, "(service.pid=" + m_servicePID + ")");

        testBlobStoreWithContext(instance.getContext(), containerName, "blob1", 1);

        m_blobstoreServiceFactoryConfiguration.delete();

        // wait until a ServiceEvent UNREGISTERING is fired...
        assertTrue(removedLatch.await(5, TimeUnit.SECONDS));

        serviceRefs =
            context.getServiceReferences(BlobStoreService.class.getName(), "(service.pid=" + m_servicePID + ")");

        assertNull("Found service references for removed service?", serviceRefs);
    }

    /**
     * Tests that we can obtain a {@link BlobStoreContext} API object from our
     * blob store service, and interact with it.
     */
    public void testGetBlobStoreServiceContextOk() {
        BlobStoreContext bsContext = instance.getContext();
        assertNotNull(bsContext);

        assertNotNull(bsContext.getBlobStore());
    }

    /**
     * Tests that we can reuse a {@link BlobStoreContext} API object for
     * multiple requests, even after each request closes the context.
     */
    public void testReuseBlobStoreServiceContextOk() {
        String containerName = "container" + System.nanoTime();

        testBlobStoreWithContext(instance.getContext(), containerName, "blob1", 1);
        testBlobStoreWithContext(instance.getContext(), containerName, "blob2", 2);
        testBlobStoreWithContext(instance.getContext(), containerName, "blob3", 3);
    }

    /**
     * Tests that we can update the configuration of our blob store service.
     */
    public void testUpdateBlobStoreServiceConfigurationOk() throws Exception {
        final String transientProvider = BlobStoreService.Providers.TRANSIENT.toString();
        String containerName = "container" + System.nanoTime();

        testBlobStoreWithContext(instance.getContext(), containerName, "blob1", 1);

        final CountDownLatch modifiedLatch = new CountDownLatch(1);

        m_listener = new ServiceListener() {
            @Override
            public void serviceChanged(ServiceEvent event) {
                if (event.getType() == ServiceEvent.MODIFIED) {
                    ServiceReference<?> serviceRef = event.getServiceReference();

                    assertEquals("test", serviceRef.getProperty("id"));
                    assertEquals(transientProvider, serviceRef.getProperty("provider"));
                    assertEquals("otherUser", serviceRef.getProperty("identity"));

                    modifiedLatch.countDown();
                }
            }
        };

        context.addServiceListener(m_listener, "(service.pid=" + m_servicePID + ")");

        Configuration configuration = getConfiguration(m_servicePID);
        Dictionary<String, Object> props = new Hashtable<>();
        props.put("id", "test");
        props.put("provider", BlobStoreService.Providers.TRANSIENT.toString());
        props.put("identity", "otherUser");
        props.put("secret", "otherPassword");
        configuration.update(props);

        // wait until a ServiceEvent MODIFIED is fired...
        assertTrue(modifiedLatch.await(5, TimeUnit.SECONDS));

        // context is recreated, so our changes are lost for a transient provider...
        testBlobStoreWithContext(instance.getContext(), containerName, "blob2", 1);
    }

    @Override
    protected void tearDown() throws Exception {
        if (m_listener != null) {
            context.removeServiceListener(m_listener);
        }
        m_listener = null;
        deleteConfiguration(m_servicePID);
        cleanUp(this);

        super.tearDown();
    }

    private void testBlobStoreWithContext(BlobStoreContext context, String containerName, String name, int expectedSize) {
        PageSet<? extends StorageMetadata> list;

        try {
            BlobStore blobStore = context.getBlobStore();

            Location location = null;
            /*
             * For AWS-S3, use something like:
             * Location provider = new LocationBuilder().scope(LocationScope.PROVIDER).id("aws-ec2").description("aws-ec2").build();
             * location = new LocationBuilder().scope(LocationScope.REGION).id("sa-east-1").description("sa-east-1").parent(provider).build();
             */
            blobStore.createContainerInLocation(location, containerName);

            list = blobStore.list(containerName);
            assertEquals(expectedSize - 1, list.size());

            byte[] bytes = name.getBytes();
            ByteArrayInputStream is = new ByteArrayInputStream(bytes);

            blobStore.putBlob(containerName,
                blobStore.blobBuilder(name)
                    .payload(is)
                    // Many provides require the content length to be set!
                    .contentLength(bytes.length).build());

            list = blobStore.list(containerName);
            assertEquals(expectedSize, list.size());

            assertTrue("Blob does not exist?!", blobStore.blobExists(containerName, name));
        } finally {
            context.close();
        }
    }
}
